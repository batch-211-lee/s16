console.log("Hello World");

//Arithmetic Operators

 	let x = 1397;
 	let y = 7831;

 	let sum = x + y;
 	console.log("Results of addition operato: " + sum);
 	let difference = x - y;
 	console.log("Result of subtraction operator: " + difference);
 	let product = x * y;
 	console.log("Result of multiplication operator: " + product);
 	let quotient = x / y;
 	console.log("Result of division operator: " + quotient);
 	let remainder = y % x;
 	console.log("Result of modulo operator: " + remainder);

//Assignment Operators ( = )

	//Basic Assignment Operator

	let assignmentNumber = 8;

	//Addition Assignment Operator ( += )
	//Addition Assignment Operator adds the value of the right operand to a variable and assigns the results to a the variable

	assignmentNumber = assignmentNumber + 2;
	console.log("Results of addition operator: " + assignmentNumber);

	assignmentNumber += 2;
	console.log("Results of addition assignment operator: " + assignmentNumber);

	//Subtraction/Multiplication/Division Assignment Operator (-=, *=, /= )

	assignmentNumber -= 2;
	console.log("Results of subtraction assignment operator: " + assignmentNumber);

	assignmentNumber *= 2;
	console.log("Results of multiplication assignment operator: " + assignmentNumber);

	assignmentNumber /= 2;
	console.log("Results of division assignment operator: " + assignmentNumber);

	//Multiple Operators and Parenthese
		/*
			-When multiple operators are applied in a single statement, it follows the PEMDAS (Parentheses, Exponents, Multiplication, Division, Addition, Substraction) rule

		*/

		let mdas =  1 + 2 - 3 * 4 / 5;
		console.log("Results of MDAS operation: " + mdas);

		let pemdas = 1 + (2 - 3) * (4 / 5);
		console.log("Results of PEMDAS operation: " + pemdas);

		/*
			-By adding parenthese "()" to create more complex computations will change the order of the operations still following the same rule
		*/ 

//Increment and Decrement (++, --)
	//Operators that add or subtract values by 1 and reassigns the value of the variable where the increment/decrement was applied to

	let z = 1;
	//the value of "z" is added by a value of one before returning the value and storing it in the variable "increment"
	let increment = ++z;
	console.log("Results of pre-increment: " + increment);
	console.log("Results of pre-increment: " + z);

	//the value of "z" is returned and stored in the variable "increment" then the value of "z" is increased by 1

	increment = z++;

	//The value of "z" is at 2 before it was incremented
	console.log("Results of post-increment: " + increment);
	//the value of "z" was increased again, re-assigning the value to 3
	console.log ("Results of post-increment: " + z);

	let decrement = --z;
	console.log("Results of pre-decrement: " + decrement);
	console.log("Results of pre-decrement: " + z);

	decrement = z--;
		console.log("Results of post-decrement: " + decrement);
	console.log("Results of post-decrement: " + z);

	//Type Coercion

	let numA = "10";
	let numB = 12;

	/*
		-adding a string and a number will result in a string
		-this can be proven in the console by looking at the color of the text displayed
	*/

	let coercion = numA + numB;
	console.log(coercion);
	console.log(typeof coercion);

	let numC = 16;
	let numD = 14;

	/*
	-the result is a number
	-this can be proven in the console by looking at the color of the text
	-blue text means output returned is a number data type
	*/

	let nonCoercion = numC + numD;
	console.log(nonCoercion);
	console.log(typeof nonCoercion);

	/*
	-the result is a number
	-the boolean "true" is also associated with the value of 1
	*/

	let numE = true + 1;
	console.log(numE);

	/*
	-the result is a number
	-the boolean "false" is also associated with the value of 1
	*/

	let numF = false + 1;
	console.log(numF);

	//Comparison Operators

	let juan = "juan";

	//Equality Operator (==)

	/*
		-checks whether the operands are equal/have the same content
		-attempts to CONVERT and COMPARE operands of different data types
		-returns a boolean value
	*/

	console.log(1 == 1); //true
	console.log(1 == 2); //false
	console.log(1 == "1"); //true
	console.log(0 == false); //true
	console.log('juan' == 'juan');//true
	console.log("juan" == juan);//true

	//Inequality Operator

	/*
		-checks whether the operands are not equal/ have different content
		-attempts to CONVERT and COMPARE operands of different data type
	*/

	console.log(1 != 1); //false
	console.log(1 != 2); //true
	console.log(1 != "1"); //false
	console.log(0 != false); //false
	console.log('juan' != 'juan');//false
	console.log("juan" != juan);//false

	//Strict Equality Operator

	/*
		-checks whether the operands are equal/have the same content
		-also it COMPARES the data types of the two values
		-JavaScript is a loosely tpes language meaning that values of different data types can be stores in variables
		-strict equality operators are better to use in most cases to ensure that data types are correct
	*/

	console.log(1 === 1); //true
	console.log(1 === 2); //false
	console.log(1 === "1"); //false
	console.log(0 === false); //false
	console.log('juan' === 'juan');//true
	console.log("juan" === juan);//true

	//Strict Inequality Operator (!==)

	/*
		-checks whether the operands are not equal/don't have the same content
		-also it COMPARES the data types of the two values
	*/

	console.log(1 !== 1); //false
	console.log(1 !== 2); //true
	console.log(1 !== "1"); //true
	console.log(0 !== false); //true
	console.log('juan' !== 'juan');//false
	console.log("juan" !== juan);//false

	//Relational Operators

	let a = 50;
	let b = 65;

	let isGreaterThan = a > b;
	let isLessThan = a < b;
	let isGTorEqual = a >= b;
	let isLTorEqual = a <= b;

	console.log(isGreaterThan); //false
	console.log(isLessThan); //true
	console.log(isGTorEqual); //false
	console.log(isLTorEqual); //true

	let numStr = "30";
	//coercion to change the string to a number
	console.log(a > numStr);//true
	console.log( b <= numStr);//false

	let str = "twenty";
	console.log( b >= str);//false
	//since string is not numeric
	//the string was converted to a number
	//result to NaN. 65 is not greater than NaN

	//Logical Operator (&&, ||, !)

	let isLegalAge = true;
	let isRegistered = false;

	//Logical AND Operator (&& - Double Ampersand)
	//Return true if all operands are true
	let allRequirementsMet = isLegalAge && isRegistered;
	console.log("Result of logical AND Operator: " + allRequirementsMet);//false

	//Logical OR Operator (|| - double pipe)
	//Returns true if one of the operands are true

	let someRequirementsMet = isLegalAge || isRegistered;
	console.log("Result of logical OR Operator: " + someRequirementsMet); //true

	//Logical NOT Operator (! - exclamation point)
	//Returns the opposite value

	let someRequirementsNotMet = !isRegistered;
	console.log("Results of logical NOT Operato: " + someRequirementsNotMet);

	//"typeof" operator is used to check the data type of a value/expression and returns a string value of what the data type is